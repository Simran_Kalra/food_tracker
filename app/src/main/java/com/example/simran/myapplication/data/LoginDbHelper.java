package com.example.simran.myapplication.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by simran on 27/3/19.
 */

public class LoginDbHelper extends SQLiteOpenHelper {
    public static final String LOG_TAG = LoginDbHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "User.db";


    public LoginDbHelper(Context context) {
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_USER_TABLE = "CREATE TABLE " + LoginContract.LoginEntry.UTABLE_NAME + "("
                + LoginContract.LoginEntry._UID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + LoginContract.LoginEntry.UCOLUMN_NAME + " TEXT NOT NULL, "
                + LoginContract.LoginEntry.UCOLUMN_HEIGHT + " INTEGER NOT NULL, "
                + LoginContract.LoginEntry.UCOLUMN_WEIGHT + " INTEGER NOT NULL, "
                + LoginContract.LoginEntry.UCOLUMN_AGE + " INTEGER NOT NULL, "
                + LoginContract.LoginEntry.UCOLUMN_GENDER + " TEXT NOT NULL );";
        db.execSQL(CREATE_USER_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + LoginContract.LoginEntry.UTABLE_NAME);
        // Create tables again
        onCreate(db);
    }
}
