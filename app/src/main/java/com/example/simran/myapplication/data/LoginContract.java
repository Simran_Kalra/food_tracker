package com.example.simran.myapplication.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by simran on 27/3/19.
 */

public class LoginContract {

    private LoginContract(){}
    public static final String CONTENT_AUTHORITY = "com.example.simran.myapplication";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_USERS = "User";




    public static abstract class LoginEntry implements BaseColumns {
        /** The content URI to access the pet data in the provider */
        public static final Uri UCONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_USERS);


        /**
         * The MIME type of the {@link #CONTENT_URI} for a list of pets.
         */
        public static final String UCONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_USERS;


        /**
         * The MIME type of the {@link #CONTENT_URI} for a single pet.
         */
        public static final String UCONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_USERS;


        public static final String UTABLE_NAME = "User";
        public static final String _UID = BaseColumns._ID;
        public static final String UCOLUMN_NAME = "name";
        public static final String UCOLUMN_AGE = "age";
        public static final String UCOLUMN_HEIGHT = "height";
        public static final String UCOLUMN_WEIGHT = "weight";
        public static final String UCOLUMN_GENDER = "gender";
    }
}
