package com.example.simran.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.simran.myapplication.data.LoginContract;
import com.example.simran.myapplication.data.LoginDbHelper;

public class MainActivity extends AppCompatActivity{
    String name;
    String gender;
    String str_age;
    int age;
    String str_height;
    int height;
    String str_weight;
    int weight;
    private EditText ed_name;
    private EditText ed_age;
    private EditText ed_height;
    private EditText ed_weight;
    private EditText ed_gender;
    private Uri mCurrentStudentUri=null;
    SharedPreferences sharedpreferences;
    public static final String Name = "nameKey";
    public static final String Gender = "genderKey";
    public static final String Age = "ageKey";
    public static final String Height = "heightKey";
    public static final String Weight = "weightKey";
    public static final String mypreference = "mypref";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ed_name=(EditText) findViewById(R.id.ed_name);
        ed_age=(EditText) findViewById(R.id.ed_age);
        ed_height=(EditText) findViewById(R.id.ed_height);
        ed_weight=(EditText) findViewById(R.id.ed_weight);
        ed_gender=(EditText) findViewById(R.id.ed_gender);
        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Name)) {
            ed_name.setText(sharedpreferences.getString(Name, ""));
        }
        if (sharedpreferences.contains(Gender)) {
            ed_gender.setText(sharedpreferences.getString(Gender, ""));

        }
        if (sharedpreferences.contains(Age)) {
            ed_age.setText(String.valueOf(sharedpreferences.getInt(Age, 0)));
        }
        if (sharedpreferences.contains(Height)) {
            ed_height.setText(String.valueOf(sharedpreferences.getInt(Height, 0)));

        }
        if (sharedpreferences.contains(Weight)) {
            ed_weight.setText(String.valueOf(sharedpreferences.getInt(Weight, 0)));
        }
    }
    public void addDetails(View view){
        name=ed_name.getText().toString().trim();
        str_age=ed_age.getText().toString().trim();
        age=Integer.parseInt(str_age);
        str_height=ed_height.getText().toString().trim();
        height=Integer.parseInt(str_height);
        str_weight=ed_weight.getText().toString().trim();
        weight=Integer.parseInt(str_weight);
        gender=ed_gender.getText().toString().trim();

        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Create a ContentValues object where column names are the keys,
        // and Toto's pet attributes are the values.
        ContentValues values = new ContentValues();
        values.put(LoginContract.LoginEntry.UCOLUMN_NAME, name);
        values.put(LoginContract.LoginEntry.UCOLUMN_AGE,age);
        values.put(LoginContract.LoginEntry.UCOLUMN_HEIGHT, height);
        values.put(LoginContract.LoginEntry.UCOLUMN_WEIGHT, weight);
        values.put(LoginContract.LoginEntry.UCOLUMN_GENDER, gender);



        if(mCurrentStudentUri==null){
            Uri newUri = getContentResolver().insert(LoginContract.LoginEntry.UCONTENT_URI, values);

            // Show a toast message depending on whether or not the insertion was successful
            if (newUri == null) {
                // If the new content URI is null, then there was an error with insertion.
                Toast.makeText(this, getString(R.string.editor_insert_user_failed),
                        Toast.LENGTH_SHORT).show();
            }
            else {
                // Otherwise, the insertion was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_insert_user_successful),
                        Toast.LENGTH_SHORT).show();
                save();
            }
        }
        else{
            int rowsAffected = getContentResolver().update(mCurrentStudentUri, values, null, null);

            // Show a toast message depending on whether or not the update was successful.
            if (rowsAffected == 0) {
                // If no rows were affected, then there was an error with the update.
                Toast.makeText(this, getString(R.string.editor_update_user_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the update was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_update_user_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void save() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Name, name);
        editor.putInt(Age, age);
        editor.putInt(Height, height);
        editor.putInt(Weight, weight);
        editor.putString(Gender, gender);
        editor.commit();
    }
}
